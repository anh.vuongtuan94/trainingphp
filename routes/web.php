<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function(){
    if (Auth::check()) {
        return view('login');
    }else{
        return redirect()->route('quanhuyen.index');
    }
});


Route::group(['prefix'=>'/','middleware'=>'auth'], function(){
    Route::group(['prefix'=>'quanhuyen'], function(){
        Route::get('list-quanhuyen', 'DistrictController@index')->name('quanhuyen.index');
        Route::get('quanhuyen-creat', 'DistrictController@creat')->name('quanhuyen.creat');
        Route::post('quanhuyen-postcreat', 'DistrictController@postcreat')->name('quanhuyen.postcreat');
        Route::get('quanhuyen-edit/{id}', 'DistrictController@edit')->name('quanhuyen.edit');
        Route::post('quanhuyen-postedit/{id}', 'DistrictController@postedit')->name('quanhuyen.postedit');
        Route::get('quanhuyen-del/{id}', 'DistrictController@delete')->name('quanhuyen.delete');
    });

    Route::group(['prefix'=>'tinhthanh'], function(){
        Route::get('list-tinhthanh', 'ProvinceController@index')->name('tinhthanh.index');
        Route::get('tinhthanh-creat', 'ProvinceController@creat')->name('tinhthanh.creat');
        Route::post('tinhthanh-postcreat', 'ProvinceController@postcreat')->name('tinhthanh.postcreat');
        Route::get('tinhthanh-edit/{id}', 'ProvinceController@edit')->name('tinhthanh.edit');
        Route::post('tinhthanh-postedit/{id}', 'ProvinceController@postedit')->name('tinhthanh.postedit');
        Route::get('tinhthanh-del/{id}', 'ProvinceController@delete')->name('tinhthanh.delete');
    });

    Route::get('language/{language}', 'LanguageController@index')->name('language.index');
});

Route::get('/login', 'HomeController@login');
Route::post('/login', 'HomeController@post_login')->name('login');
Route::get('/logout', 'HomeController@logout')->name('logout');





