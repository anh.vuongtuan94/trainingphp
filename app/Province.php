<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'cs_category_province';
	protected $fillable = ['id','code','fullname','short_name','start_date','end_date','created_by','created_at','updated_by','updated_at','isdeleted','deleted_by','deleted_at','isvalid'];
}
