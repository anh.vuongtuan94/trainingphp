<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\District;
use App\Http\Requests\ProvinceRequest;

class ProvinceController extends Controller
{
    public function index(Request $request)
    {
        $province = new Province();
        $searchData = [];
        if (!empty($request->search)) {
            $province = $province->where('fullname','like', "%{$request->search}%");
            $searchData['search'] = $request->search;
        }
        $province = $province->paginate(20);
        //dd($province->currentPage());
        return view('tinhthanh.index',compact('province', 'searchData'));
    }
    public function search(Request $req)
    {
        if ($req->has('search')) {
        //dd($req);
            $query = $req->search;
            $province = Province::where('fullname','like', "%{$query}%")->paginate(5);
            return view('tinhthanh.index',compact('province'));
        //dd($district);
        }

    }

    public function creat()
    {
        return view('tinhthanh.creat');
    }


    public function postcreat(ProvinceRequest $req)
    {
        if (Province::create($req->all())) {
            return redirect()->route('tinhthanh.index')->with(['status' => 'success_cre', 'message' => 'Thêm tỉnh thành thành công']);
        }
    }

    public function edit($id)
    {
        $province = Province::find($id);
        return view('tinhthanh.edit',compact('province'));
    }

    public function postedit(ProvinceRequest $req, $id)
    {
        $data = $req->all();
        $edit = Province::findOrFail($id);
        if ($edit->update($data)) {
            return redirect()->route('tinhthanh.index')->with(['status' => 'success_edit', 'message' => 'Sửa tỉnh thành thành công']);
        }
    }

    public function delete($id)
    {
        if (Province::where('id',$id)->delete()) {
            return redirect()->route('tinhthanh.index')->with(['status' => 'success_del', 'message' => 'Xóa tỉnh thành thành công']);
            //echo "Thành công";
        }
    }


}
