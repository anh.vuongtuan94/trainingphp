<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


	public function login(){
        return view('login');
	}

	public function post_login(Request $req){
		$this->validate($req,[
			'email' => 'required|email',
			'password' => 'required'
		],[
			'email.required' => 'Email ko được để trống',
			'email.email' => 'Email ko đúng định dạng',
			'password.required' => 'Mật khẩu ko được để trống'
		]);
		//thực hiện login
		if (Auth::attempt($req->only('email','password'))) {
			return redirect()->route('tinhthanh.index');
		}else{
			return redirect()->back();
		}
    }



    public function logout(){
        Auth::logout();
		return redirect()->route('login');
	}
}
