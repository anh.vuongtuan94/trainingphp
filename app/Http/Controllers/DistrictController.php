<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\District;
use App\Province;
use App\Http\Requests\DistrictRequest;

class DistrictController extends Controller
{



    public function index(Request $request)
    {
        $district = new District();
        $searchData = [];
        if (!empty($request->search)) {
            $district = $district->where('fullname','like', "%{$request->search}%")->orwhere('code','like', "%{$request->search}%");
            $searchData['search'] = $request->search;
        }
        if (!empty($request->province_id)) {
            $district = $district->where('province_id','=', $request->province_id);
            $searchData['province_id'] = $request->province_id;
        }
        if (!empty($request->created_at)) {
            $district = $district->where('created_at','>=',$request->created_at);
            $searchData['created_at'] = $request->created_at;
        }
        $searchData['record'] = $request->record;
        $district = $district->paginate($request->record);
        $province = Province::all();
        //dd($district);
        //dd($searchData);
        return view('quanhuyen.index',compact('district', 'province', 'searchData'));
    }

    public function creat()
    {
        $province = Province::all();
        return view('quanhuyen.creat',compact('province'));
    }


    public function postcreat(DistrictRequest $req)
    {
        if (District::create($req->all())) {
            return redirect()->route('quanhuyen.index')->with(['status' => 'success_cre', 'message' => 'Thêm quận huyện thành công']);
        }
    }

    public function edit($id)
    {
        $dist = District::find($id);
        $province = Province::all();
        return view('quanhuyen.edit',compact('province','dist'));
    }

    public function postedit(DistrictRequest $req, $id)
    {
        $data = $req->all();
        $edit = District::findOrFail($id);
        if ($edit->update($data)) {
            return redirect()->route('quanhuyen.index')->with(['status' => 'success_edit', 'message' => 'Sửa quận huyện thành công']);
        }
    }

    public function delete($id)
    {
        if (District::where('id',$id)->delete()) {
            return redirect()->route('quanhuyen.index')->with(['status' => 'success_del', 'message' => 'Xóa quận huyện thành công']);
            //echo "Thành công";
        }
    }

}
