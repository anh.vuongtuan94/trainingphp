<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProvinceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|max:15',
            'fullname' => 'required',
            // 'sdt_nguoilienhe' => [
            //     'required',
            //     function ($attribute, $value, $fail) {
            //         if (preg_match('/^[0-9_~\-+!@#\$%\^,.&\*\(\) ]+$/', $value) == 0) {
            //             return $fail('Số điện thoại sai định dạng');
            //         }
            //     }
            // ],
        ];


        return $rules;
    }

    public function messages()
    {
        return [
            'code.required' => 'Mã tỉnh thành không được bỏ trống',
            'code.max' => 'Mã tỉnh thành phải nhỏ hơn 15 ký tự ',
            'fullname.required' => 'Tên tỉnh thành không được bỏ trống',
        ];
    }
}
