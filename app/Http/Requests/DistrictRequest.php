<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistrictRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|max:15',
            'fullname' => 'required',
            'province_id' => 'required',
            // 'sdt_nguoilienhe' => [
            //     'required',
            //     function ($attribute, $value, $fail) {
            //         if (preg_match('/^[0-9_~\-+!@#\$%\^,.&\*\(\) ]+$/', $value) == 0) {
            //             return $fail('Số điện thoại sai định dạng');
            //         }
            //     }
            // ],
        ];


        return $rules;
    }

    public function messages()
    {
        return [
            'code.required' => 'Mã quận huyện không được bỏ trống',
            'code.max' => 'Mã quận huyện phải nhỏ hơn 15 ký tự ',
            'fullname.required' => 'Tên quận huyên không được bỏ trống',
            'province_id.required' => 'Chưa chọn tỉnh thành',
        ];
    }
}
