<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $table = 'cs_category_district';
	protected $fillable = ['id','code','fullname','short_name','start_date','end_date','created_by','created_at','updated_by','updated_at','isdeleted','deleted_by','deleted_at','isvalid','province_id','new_code'];
}
