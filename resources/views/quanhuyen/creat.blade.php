@extends('layouts.all')

@section('main')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <div class="row layout-top-spacing" id="cancel-row">
                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div><h3>{{__('message.createDistrict')}}</h3></div>
                    <div class="widget-content widget-content-area br-6">
                        <form action="{{route('quanhuyen.postcreat')}}" method="post" role="form" style="width: 100%">
                            @csrf
                            <div class="form-row mb-4">
                                <div class="col">
                                    <label style="justify-content: left">Mã quận huyện</label>
                                    <input  maxlength="255" type="text" class="form-control " id="code" name="code" value="" placeholder="">
                                    @if($errors->has('code'))
                                        <div style="color: red">
                                            {{ $errors->first('code') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Tên viết tắt</label>
                                    <input style="width: 100%" maxlength="255" type="text" class="form-control " id="short_name" name="short_name" value="" placeholder="">
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Tên quận huyện</label>
                                    <input style="width: 100%" maxlength="255" type="text" class="form-control " id="fullname" name="fullname" value="" placeholder="">
                                    @if($errors->has('fullname'))
                                        <div style="color: red">
                                            {{ $errors->first('fullname') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row mb-4">
                                <div class="col">
                                    <label style="justify-content: left">Tên quận huyện</label>
                                    <select class="form-control form-control-lg"name="province_id" id="province_id" style="height: 45px">
                                        <option value="">Chọn tỉnh thành</option>
                                        @foreach ($province as $pro)
                                            <option value="{{$pro->code}}">{{$pro->fullname}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('province_id'))
                                        <div style="color: red">
                                            {{ $errors->first('province_id') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Ngày bắt đầu</label>
                                    <input style="width: 100%" type="date" class="form-control " id="start_date" name="start_date" value="" placeholder="">
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Ngày kết thúc</label>
                                    <input style="width: 100%" type="date" class="form-control " id="end_date" name="end_date" value="" placeholder="">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                {{__('message.createButton')}}
                            </button>
                        <a href="{{route('quanhuyen.index')}}" class="btn btn-danger">{{__('message.backButton')}}</a>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop()




