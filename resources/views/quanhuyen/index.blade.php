@extends('layouts.all')


@section('main')



        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                @if(\Session::get('status'))
                    <script>
                        var message = '{{\Session::get('message')}}';
                        var status = '{{\Session::get('status')}}';
                        swal(message, "", {
                            button: "Đóng",
                            timer: 10000
                        });
                    </script>
                @endif

                <div class="row layout-top-spacing" id="cancel-row">

                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div><h3>{{__('message.district')}}</h3></div>
                        <div class="widget-content widget-content-area br-6">
                            <form action="{{route('quanhuyen.index')}}" method="get" role="form" style="width: 100%">
                                @csrf
                                <div class="form-row mb-4">
                                    <div class="col">
                                    <input  maxlength="255" type="text" class="form-control " id="search_input" name="search" value="{{!empty($searchData['search']) ? $searchData['search'] : ''}}" placeholder="Nhập tên quận huyện">
                                    </div>
                                    <div class="col">
                                        <select class="form-control form-control-lg" name="province_id" id="province_id" style="height: 45px">
                                            <option value="">Chọn tỉnh thành</option>
                                            @foreach ($province as $pro)
                                                <option {{!empty($searchData['province_id']) ? $searchData['province_id'] == $pro->code ? 'selected' : '' : ''}} value="{{$pro->code}}">{{$pro->fullname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <input value="{{!empty($searchData['created_at']) ? $searchData['created_at'] : ''}}" type="date" class="form-control " id="" name="created_at" >
                                    </div>
                                    <div class="col">
                                            <select class="form-control form-control-lg" name="record" id="record" style="height: 45px">
                                                <option {{!empty($searchData['record']) ? $searchData['record'] == 15 ? 'selected' : '' : ''}} value="15">15</option>
                                                <option {{!empty($searchData['record']) ? $searchData['record'] == 30 ? 'selected' : '' : ''}} value="30">30</option>
                                                <option {{!empty($searchData['record']) ? $searchData['record'] == 50 ? 'selected' : '' : ''}} value="50">50</option>
                                            </select>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary">
                                            {{__('message.searchButton')}}
                                        </button>
                                        <a href="{{route('quanhuyen.creat')}}" class="btn btn-success">{{__('message.createButton')}}</a>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive mb-4 mt-4">
                                <table id="zero-config" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã quận huyện</th>
                                            <th>Tên viết tắt</th>
                                            <th>Tên quận huyện</th>
                                            <th>Tỉnh thành</th>
                                            <th>Hiệu lực</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($district as $distric => $dist)
                                        @php
                                            $stt = $district->perPage() *  ($district->currentPage() -1) + $distric + 1
                                        @endphp
                                        <tr >
                                        <td >{{ $stt}}</td>
                                        <td >{{ $dist->code }}</td>
                                        <td >{{ $dist->short_name }}</td>
                                            <td >{{ $dist->fullname }}</td>
                                            <td>
                                                @foreach ($province as $pro)
                                                    @if ($pro->code == $dist->province_id)
                                                        {{ $pro->fullname }}
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td >
                                                @switch($dist->isvalid)
                                                @case(0)
                                                    {{ "Còn hiệu lực" }}
                                                    @break
                                                @case(1)
                                                    {{ "Hết hiệu lực" }}
                                                    @break
                                                @endswitch
                                            </td>
                                            <td >
                                            <a href="{{route('quanhuyen.edit',['id'=>$dist->id])}}" class="btn btn-info">{{__('message.editButton')}}</a>
                                                <a href="{{route('quanhuyen.delete',['id'=>$dist->id])}}" class="btn btn-danger">{{__('message.deleteButton')}}</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã quận huyện</th>
                                            <th>Tên viết tắt</th>
                                            <th>Tên quận huyện</th>
                                            <th>Tỉnh thành</th>
                                            <th>Hiệu lực</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div style="text-align: right; display: flex" >
                                <div >{!! $district->appends(request()->input())->links() !!}</div>
                            </div>
                        </div>
                    </div>


                </div>

                </div>


@stop()




