@extends('layouts.all')

@section('main')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <div class="row layout-top-spacing" id="cancel-row">
                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div><h3>THÊM MỚI TỈNH THÀNH</h3></div>
                    <div class="widget-content widget-content-area br-6">
                        <form action="{{route('tinhthanh.postcreat')}}" method="post" role="form" style="width: 100%">
                            @csrf
                            <div class="form-row mb-4">
                                <div class="col">
                                    <label style="justify-content: left">Mã tỉnh thành</label>
                                    <input style="width: 100%" maxlength="255" type="text" class="form-control " id="code" name="code" value="" placeholder="">
                                    @if($errors->has('code'))
                                        <div style="color: red">
                                            {{ $errors->first('code') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Tên viết tắt</label>
                                    <input style="width: 100%" maxlength="255" type="text" class="form-control " id="short_name" name="short_name" value="" placeholder="">
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Tên tỉnh thành</label>
                                    <input style="width: 100%" maxlength="255" type="text" class="form-control " id="fullname" name="fullname" value="" placeholder="">
                                    @if($errors->has('fullname'))
                                        <div style="color: red">
                                            {{ $errors->first('fullname') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row mb-4">
                                <div class="col">
                                    <label style="justify-content: left">Ngày bắt đầu</label>
                                    <input style="width: 100%" type="date" class="form-control " id="start_date" name="start_date" value="" placeholder="">
                                </div>
                                <div class="col">
                                    <label style="justify-content: left">Ngày kết thúc</label>
                                    <input style="width: 100%" type="date" class="form-control " id="end_date" name="end_date" value="" placeholder="">
                                </div>
                                <div class="col"></div>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                Thêm mới
                            </button>
                            <a href="{{route('tinhthanh.index')}}" class="btn btn-danger">Quay lại</a>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

@stop()




