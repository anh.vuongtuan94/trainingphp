@extends('layouts.all')


@section('main')



        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                @if(\Session::get('status'))
                    <script>
                        var message = '{{\Session::get('message')}}';
                        var status = '{{\Session::get('status')}}';
                        swal(message, "", {
                            button: "Đóng",
                            timer: 10000
                        });
                    </script>
                @endif
                {{-- @dd(request()->get('page')) --}}
                <div class="row layout-top-spacing" id="cancel-row">

                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div><h3>DANH SÁCH TỈNH THÀNH</h3></div>
                        <div class="widget-content widget-content-area br-6">
                            <form action="{{route('tinhthanh.index')}}" method="get" role="form" style="width: 100%">
                                @csrf
                                <div class="form-row mb-4">
                                    <div class="col">
                                    <input  maxlength="255" type="text" class="form-control " id="search_input" name="search" value="{{!empty($searchData['search']) ? $searchData['search'] : ''}}" placeholder="Nhập tên tỉnh thành">
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary">
                                            Tìm kiếm
                                        </button>
                                        <a href="{{route('tinhthanh.creat')}}" class="btn btn-success">Thêm mới</a>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive mb-4 mt-4">
                                <table id="zero-config" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã tỉnh thành</th>
                                            <th>Tên tỉnh thành</th>
                                            <th>Tên viết tắt</th>
                                            <th>Hiệu lực</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($province as $provin => $pro)
                                        @php
                                            $stt = $province->perPage() *  ($province->currentPage() -1) + $provin + 1
                                        @endphp
                                            <tr >
                                                <td >{{ $stt}}</td>
                                                <td >{{ $pro->code }}</td>
                                                <td >{{ $pro->fullname }}</td>
                                                <td>{{ $pro->short_name }}</td>

                                                <td style="">
                                                    @switch($pro->isvalid)
                                                    @case(0)
                                                        {{ "Còn hiệu lực" }}
                                                        @break
                                                    @case(1)
                                                        {{ "Hết hiệu lực" }}
                                                        @break
                                                    @endswitch
                                                </td>
                                                <td >
                                                    <a href="{{route('tinhthanh.edit',['id'=>$pro->id])}}" class="btn btn-info">Sửa</a>
                                                    <a href="{{route('tinhthanh.delete',['id'=>$pro->id])}}" class="btn btn-danger">Xóa</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã tỉnh thành</th>
                                            <th>Tên tỉnh thành</th>
                                            <th>Tên viết tắt</th>
                                            <th>Hiệu lực</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div style="text-align: right">
                                {!! $province->appends(request()->input())->links() !!}
                            </div>
                        </div>
                    </div>


                </div>

                </div>


@stop()




