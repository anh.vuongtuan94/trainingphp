<?php

return [

   'category' => 'DANH MỤC',
   'district' => 'DANH MỤC QUẬN HUYỆN',
   'province' => 'DANH MỤC TỈNH THÀNH',
   'signout'  => 'Đăng xuất',
   'createDistrict' => 'THÊM MỚI QUẬN HUYỆN',
   'createProvince' => 'THÊM MỚI TỈNH THÀNH',
   'editDistrict' => 'CHỈNH SỬA QUẬN HUYỆN',
   'editProvince' => 'CHỈNH SỬA TỈNH THÀNH',
   'createButton' => 'Thêm mới',
   'editButton' => 'Chỉnh sửa',
   'backButton' => 'Quay lại',
   'searchButton' => 'Tìm kiếm',
   'deleteButton' => 'Xóa',
];
