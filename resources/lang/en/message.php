<?php

return [

   'category' => 'CATEGORY',
   'district' => 'DISTRICT LIST',
   'province' => 'PROVINCE LIST',
   'signout'  => 'Sign out',
   'createDistrict' => 'DISTRICT CREATE',
   'createProvince' => 'PROVINCE CREATE',
   'editDistrict' => 'EDIT DISTRICT',
   'editProvince' => 'EDIT PROVINCE',
   'createButton' => 'Create',
   'editButton' => 'Edit',
   'backButton' => 'Back',
   'searchButton' => 'Search',
   'deleteButton' => 'Delete',
];
